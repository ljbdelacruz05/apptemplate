import 'dart:ui';
import 'package:flutter/material.dart';

class ColorConstant {
  static Color black9007f = fromHex('#7f000000');

  static Color gray5001 = fromHex('#f8f9fa');

  static Color gray5002 = fromHex('#fafcff');

  static Color black900B2 = fromHex('#b2000000');

  static Color gray5003 = fromHex('#f6f7fb');

  static Color gray4004c = fromHex('#4cc4c4c4');

  static Color red400 = fromHex('#f14a60');

  static Color greenA100 = fromHex('#b5eacd');

  static Color black90000 = fromHex('#00000000');

  static Color lightBlueA700 = fromHex('#007aff');

  static Color blueGray90002 = fromHex('#2e3637');

  static Color blueGray90001 = fromHex('#24363c');

  static Color blueGray900 = fromHex('#262b35');

  static Color deepPurpleA200 = fromHex('#735bf2');

  static Color redA20001 = fromHex('#fd6161');

  static Color blue900 = fromHex('#003399');

  static Color gray400 = fromHex('#b3b3b3');

  static Color blueGray100 = fromHex('#d6dae2');

  static Color amber500 = fromHex('#feb909');

  static Color deepPurple50 = fromHex('#ebe8f1');

  static Color black90011 = fromHex('#11000000');

  static Color black90099 = fromHex('#99000000');

  static Color whiteA70067 = fromHex('#67ffffff');

  static Color deepOrangeA10019 = fromHex('#19dfa874');

  static Color gray7000f = fromHex('#0f555555');

  static Color red700 = fromHex('#d03329');

  static Color blueGray50 = fromHex('#eaecf0');

  static Color blueGray10087 = fromHex('#87ced3de');

  static Color blueA700 = fromHex('#0061ff');

  static Color green600 = fromHex('#349765');

  static Color gray50 = fromHex('#f9fbff');

  static Color blueGray20001 = fromHex('#adb5bd');

  static Color blueGray20000 = fromHex('#00bac1ce');

  static Color blueGray80002 = fromHex('#2f4254');

  static Color black90066 = fromHex('#66000000');

  static Color gray50001 = fromHex('#a6a6a6');

  static Color gray50002 = fromHex('#959494');

  static Color gray40033 = fromHex('#33b2b2b2');

  static Color deepOrange400 = fromHex('#d58c48');

  static Color gray70011 = fromHex('#11555555');

  static Color whiteA700E5 = fromHex('#e5ffffff');

  static Color blueGray200 = fromHex('#bac1ce');

  static Color gray500 = fromHex('#9e9e9e');

  static Color blue800 = fromHex('#0051d5');

  static Color blueGray600 = fromHex('#5f6c86');

  static Color gray900 = fromHex('#09101d');

  static Color blueGray80001 = fromHex('#37474f');

  static Color gray100 = fromHex('#f4f5f7');

  static Color gray70026 = fromHex('#26555555');

  static Color lightBlue100 = fromHex('#b0e5fc');

  static Color yellow9003f = fromHex('#3feb9612');

  static Color red200 = fromHex('#fa9a9a');

  static Color blueA70063 = fromHex('#630061ff');

  static Color blueA200 = fromHex('#588af1');

  static Color black9003f = fromHex('#3f000000');

  static Color gray30099 = fromHex('#99e4e4e4');

  static Color whiteA70099 = fromHex('#99ffffff');

  static Color gray20001 = fromHex('#ececec');

  static Color blueGray700 = fromHex('#424c5d');

  static Color redA700 = fromHex('#d80027');

  static Color black9004c = fromHex('#4c000000');

  static Color orangeA200 = fromHex('#ef9e3b');

  static Color black9004d = fromHex('#4d000000');

  static Color blue700 = fromHex('#1976d2');

  static Color blueGray300 = fromHex('#9ea8ba');

  static Color redA200 = fromHex('#fe555d');

  static Color gray200 = fromHex('#efefef');

  static Color blue50 = fromHex('#e0ebff');

  static Color blueGray1006c = fromHex('#6cd1d3d4');

  static Color gray10001 = fromHex('#fbf1f2');

  static Color blueGray40002 = fromHex('#888888');

  static Color blueGray40001 = fromHex('#75839d');

  static Color whiteA700 = fromHex('#ffffff');

  static Color blueGray70001 = fromHex('#535763');

  static Color gray60019 = fromHex('#197e7e7e');

  static Color red500 = fromHex('#ff3b30');

  static Color gray900A5 = fromHex('#a5070b2e');

  static Color black900 = fromHex('#000000');

  static Color blue507f = fromHex('#7fe0ebff');

  static Color blueGray800 = fromHex('#25325a');

  static Color blue5001 = fromHex('#e0ecff');

  static Color gray90002 = fromHex('#212529');

  static Color gray90003 = fromHex('#0d062d');

  static Color gray90004 = fromHex('#1e1b14');

  static Color gray90005 = fromHex('#151522');

  static Color blueGray400 = fromHex('#74839d');

  static Color orangeA700 = fromHex('#ff6700');

  static Color indigo50 = fromHex('#e9eef8');

  static Color gray90001 = fromHex('#070b2e');

  static Color blueGray30000 = fromHex('#009ea8ba');

  static Color gray90006 = fromHex('#202525');

  static Color gray90007 = fromHex('#0f0e17');

  static Color gray300 = fromHex('#d2efe0');

  static Color blueGray30001 = fromHex('#8f9bb3');

  static Color gray90008 = fromHex('#2a2a2a');

  static Color deepOrangeA10033 = fromHex('#33dfa874');

  static Color whiteA70000 = fromHex('#00ffffff');

  static Color black90033 = fromHex('#33000000');

  static Color indigo900 = fromHex('#002055');

  static Color blue200 = fromHex('#a6c8ff');

  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
