class ImageConstant {
  static String imgArrowrightBlueA700 =
      'assets/images/img_arrowright_blue_a700.svg';

  static String imgGif1WhiteA700 = 'assets/images/img_gif1_white_a700.svg';

  static String imgDealimage154x190 = 'assets/images/img_dealimage_154x190.png';

  static String imgPngwing55x321 = 'assets/images/img_pngwing_55x32_1.png';

  static String imgRectangle1320160x3001 =
      'assets/images/img_rectangle1320_160x300_1.png';

  static String imgUnsplashfr2iwkpsiy50x505 =
      'assets/images/img_unsplashfr2iwkpsiy_50x50_5.png';

  static String imgVector44 = 'assets/images/img_vector44.svg';

  static String imgFile12x12 = 'assets/images/img_file_12x12.svg';

  static String imgCamera24x24 = 'assets/images/img_camera_24x24.svg';

  static String imgRectangle1314150x1903 =
      'assets/images/img_rectangle1314_150x190_3.png';

  static String imgStar25 = 'assets/images/img_star2_5.svg';

  static String imgCar = 'assets/images/img_car.svg';

  static String imgRectangle132893x932 =
      'assets/images/img_rectangle1328_93x93_2.png';

  static String imgMinussolid = 'assets/images/img_minussolid.svg';

  static String imgGroup3628 = 'assets/images/img_group3628.svg';

  static String imgSettings24x24 = 'assets/images/img_settings_24x24.svg';

  static String imgRectangle18130x1303 =
      'assets/images/img_rectangle18_130x130_3.png';

  static String imgGraphpie = 'assets/images/img_graphpie.svg';

  static String imgArrowgrowthsolidRed700 =
      'assets/images/img_arrowgrowthsolid_red_700.svg';

  static String imgProfileimglarge42 =
      'assets/images/img_profileimglarge_42.png';

  static String imgPlusBlueGray400 = 'assets/images/img_plus_blue_gray_400.svg';

  static String imgPlus1 = 'assets/images/img_plus_1.svg';

  static String imgProfileimglarge15 =
      'assets/images/img_profileimglarge_15.png';

  static String imgRectangle1377 = 'assets/images/img_rectangle1377.png';

  static String imgInstagram = 'assets/images/img_instagram.svg';

  static String imgRectangle1312160x1601 =
      'assets/images/img_rectangle1312_160x160_1.png';

  static String imgThumbsup2 = 'assets/images/img_thumbsup_2.svg';

  static String imgTwitter20x20 = 'assets/images/img_twitter_20x20.svg';

  static String imgCloudoutlineBlueGray400 =
      'assets/images/img_cloudoutline_blue_gray_400.svg';

  static String imgDashboard24x24 = 'assets/images/img_dashboard_24x24.svg';

  static String imgProfileimglarge36 =
      'assets/images/img_profileimglarge_36.png';

  static String imgPlus16x16 = 'assets/images/img_plus_16x16.svg';

  static String imgRectangle1 = 'assets/images/img_rectangle1.png';

  static String imgEllipse750x503 = 'assets/images/img_ellipse7_50x50_3.png';

  static String imgGrid = 'assets/images/img_grid.svg';

  static String imgArrowdown12x12 = 'assets/images/img_arrowdown_12x12.svg';

  static String imgProfileimglarge26 =
      'assets/images/img_profileimglarge_26.png';

  static String imgClose24x24 = 'assets/images/img_close_24x24.svg';

  static String imgStar27 = 'assets/images/img_star2_7.svg';

  static String imgRefresh = 'assets/images/img_refresh.svg';

  static String imgPlus40x40 = 'assets/images/img_plus_40x40.svg';

  static String imgRectangle1315463x214 =
      'assets/images/img_rectangle1315_463x214.png';

  static String imgImage120 = 'assets/images/img_image120.png';

  static String imgRectangle1302 = 'assets/images/img_rectangle1302.png';

  static String imgInfo = 'assets/images/img_info.svg';

  static String imgImage79 = 'assets/images/img_image79.png';

  static String imgFile24x24 = 'assets/images/img_file_24x24.svg';

  static String imgFrame9844 = 'assets/images/img_frame9844.svg';

  static String imgFrame9844OrangeA200 =
      'assets/images/img_frame9844_orange_a200.svg';

  static String imgUser20x20 = 'assets/images/img_user_20x20.svg';

  static String imgRectangle12250x2502 =
      'assets/images/img_rectangle12_250x250_2.png';

  static String imgVector = 'assets/images/img_vector.svg';

  static String imgArrowleft = 'assets/images/img_arrowleft.svg';

  static String imgRectangle132182x821 =
      'assets/images/img_rectangle1321_82x82_1.png';

  static String imgStar21 = 'assets/images/img_star2_1.svg';

  static String imgGroup9839 = 'assets/images/img_group9839.svg';

  static String imgThumbsup1 = 'assets/images/img_thumbsup_1.svg';

  static String imgImage125 = 'assets/images/img_image125.png';

  static String imgVideocamera64x64 = 'assets/images/img_videocamera_64x64.svg';

  static String imgRectangle132693x933 =
      'assets/images/img_rectangle1326_93x93_3.png';

  static String imgProfileimglarge28 =
      'assets/images/img_profileimglarge_28.png';

  static String imgArrowdown = 'assets/images/img_arrowdown.svg';

  static String imgGroup9854BlueA700 =
      'assets/images/img_group9854_blue_a700.png';

  static String imgImage63 = 'assets/images/img_image63.png';

  static String imgContrast = 'assets/images/img_contrast.svg';

  static String imgEllipse480x80 = 'assets/images/img_ellipse4_80x80.png';

  static String imgLocationBlueGray400 =
      'assets/images/img_location_blue_gray_400.svg';

  static String imgUnsplashenrurz62wui86x863 =
      'assets/images/img_unsplashenrurz62wui_86x86_3.png';

  static String imgProfileimglarge56x564 =
      'assets/images/img_profileimglarge_56x56_4.png';

  static String imgImage66 = 'assets/images/img_image66.png';

  static String imgProfileimglarge11 =
      'assets/images/img_profileimglarge_11.png';

  static String imgEllipse750x509 = 'assets/images/img_ellipse7_50x50_9.png';

  static String imgRectangle15130x130 =
      'assets/images/img_rectangle15_130x130.png';

  static String imgRectangle10130x1304 =
      'assets/images/img_rectangle10_130x130_4.png';

  static String imgEllipse350x50 = 'assets/images/img_ellipse3_50x50.png';

  static String imgProfileimglarge40 =
      'assets/images/img_profileimglarge_40.png';

  static String imgProfileimglarge12 =
      'assets/images/img_profileimglarge_12.png';

  static String imgStar2 = 'assets/images/img_star2.svg';

  static String imgMicrophoneoutline =
      'assets/images/img_microphoneoutline.svg';

  static String imgFilter = 'assets/images/img_filter.svg';

  static String imgRectangle132793x933 =
      'assets/images/img_rectangle1327_93x93_3.png';

  static String imgRectangle16130x1302 =
      'assets/images/img_rectangle16_130x130_2.png';

  static String imgClose = 'assets/images/img_close.svg';

  static String imgProfileimglarge50x503 =
      'assets/images/img_profileimglarge_50x50_3.png';

  static String imgMdicellphonelink = 'assets/images/img_mdicellphonelink.svg';

  static String imgUnsplashenrurz62wui86x861 =
      'assets/images/img_unsplashenrurz62wui_86x86_1.png';

  static String imgLocation16x16 = 'assets/images/img_location_16x16.svg';

  static String imgRectangle1330250x3962 =
      'assets/images/img_rectangle1330_250x396_2.png';

  static String imgVolumemuteoutline =
      'assets/images/img_volumemuteoutline.svg';

  static String imgRectangle1312174x1741 =
      'assets/images/img_rectangle1312_174x174_1.png';

  static String imgGroup9845 = 'assets/images/img_group9845.png';

  static String imgRectangle132693x931 =
      'assets/images/img_rectangle1326_93x93_1.png';

  static String imgLock = 'assets/images/img_lock.svg';

  static String imgEllipse1224x241 = 'assets/images/img_ellipse12_24x24_1.png';

  static String img8148x48 = 'assets/images/img_81_48x48.png';

  static String imgProfileimglarge50x504 =
      'assets/images/img_profileimglarge_50x50_4.png';

  static String imgPngwing55x65 = 'assets/images/img_pngwing_55x65.png';

  static String imgProfileimglarge13 =
      'assets/images/img_profileimglarge_13.png';

  static String imgProfileimglarge39 =
      'assets/images/img_profileimglarge_39.png';

  static String imgMenu = 'assets/images/img_menu.svg';

  static String imgProfileimglarge41 =
      'assets/images/img_profileimglarge_41.png';

  static String imgImage80154x1902 = 'assets/images/img_image80_154x190_2.png';

  static String imgSwipe = 'assets/images/img_swipe.svg';

  static String imgFile16x16 = 'assets/images/img_file_16x16.svg';

  static String imgPolygon1Blue50 = 'assets/images/img_polygon1_blue_50.svg';

  static String imgRectangle465144x396 =
      'assets/images/img_rectangle465_144x396.png';

  static String imgRefresh24x24 = 'assets/images/img_refresh_24x24.svg';

  static String imgProfileimglarge14 =
      'assets/images/img_profileimglarge_14.png';

  static String imgChartsmicroBlue5045x150 =
      'assets/images/img_chartsmicro_blue_50_45x150.svg';

  static String imgRectangle132793x931 =
      'assets/images/img_rectangle1327_93x93_1.png';

  static String imgEllipse10118x118 = 'assets/images/img_ellipse10_118x118.png';

  static String imgCheckmark40x40 = 'assets/images/img_checkmark_40x40.svg';

  static String imgSearchBlueGray400 =
      'assets/images/img_search_blue_gray_400.svg';

  static String imgRectangle1318416x214 =
      'assets/images/img_rectangle1318_416x214.png';

  static String imgSort = 'assets/images/img_sort.svg';

  static String imgShare = 'assets/images/img_share.svg';

  static String imgCheckmark1 = 'assets/images/img_checkmark_1.svg';

  static String imgDownload = 'assets/images/img_download.svg';

  static String imgClock = 'assets/images/img_clock.svg';

  static String imgMapsettings = 'assets/images/img_mapsettings.png';

  static String imgSettings1 = 'assets/images/img_settings_1.svg';

  static String imgFavorite24x24 = 'assets/images/img_favorite_24x24.svg';

  static String imgProfileimglarge46x46 =
      'assets/images/img_profileimglarge_46x46.png';

  static String imgRectangle1314190x3962 =
      'assets/images/img_rectangle1314_190x396_2.png';

  static String imgFacebook = 'assets/images/img_facebook.svg';

  static String imgProfileimglarge31 =
      'assets/images/img_profileimglarge_31.png';

  static String imgRectangle132993x931 =
      'assets/images/img_rectangle1329_93x93_1.png';

  static String imgStar220x20 = 'assets/images/img_star2_20x20.svg';

  static String imgVideocamera = 'assets/images/img_videocamera.svg';

  static String imgAirplane = 'assets/images/img_airplane.svg';

  static String imgFlagargentina1 = 'assets/images/img_flagargentina1.png';

  static String imgUnion = 'assets/images/img_union.svg';

  static String imgUnsplashfr2iwkpsiy50x502 =
      'assets/images/img_unsplashfr2iwkpsiy_50x50_2.png';

  static String imgProfileimglarge50x506 =
      'assets/images/img_profileimglarge_50x50_6.png';

  static String imgRectangle13130x1302 =
      'assets/images/img_rectangle13_130x130_2.png';

  static String imgEllipse750x506 = 'assets/images/img_ellipse7_50x50_6.png';

  static String imgRectangle116x16 = 'assets/images/img_rectangle1_16x16.png';

  static String imgReply = 'assets/images/img_reply.svg';

  static String imgArrowrightBlueGray400 =
      'assets/images/img_arrowright_blue_gray_400.svg';

  static String imgSearchBlueGray40024x24 =
      'assets/images/img_search_blue_gray_400_24x24.svg';

  static String imgRectangle1304 = 'assets/images/img_rectangle1304.png';

  static String imgImage80154x1903 = 'assets/images/img_image80_154x190_3.png';

  static String imgRectangle1331 = 'assets/images/img_rectangle1331.png';

  static String imgPic44x44 = 'assets/images/img_pic_44x44.png';

  static String imgPngfind55x502 = 'assets/images/img_pngfind_55x50_2.png';

  static String imgGroup9854 = 'assets/images/img_group9854.png';

  static String imgGroup1062 = 'assets/images/img_group1062.svg';

  static String imgStar12 = 'assets/images/img_star1_2.svg';

  static String imgOfferBlueGray400 =
      'assets/images/img_offer_blue_gray_400.svg';

  static String imgEllipse516x16 = 'assets/images/img_ellipse5_16x16.png';

  static String imgRectangle12250x2501 =
      'assets/images/img_rectangle12_250x250_1.png';

  static String imgGroup9824 = 'assets/images/img_group9824.svg';

  static String imgChart = 'assets/images/img_chart.png';

  static String imgEllipse150x501 = 'assets/images/img_ellipse1_50x50_1.png';

  static String imgRectangle1320160x3002 =
      'assets/images/img_rectangle1320_160x300_2.png';

  static String imgRectangle20130x1301 =
      'assets/images/img_rectangle20_130x130_1.png';

  static String imgGroup9759 = 'assets/images/img_group9759.svg';

  static String imgArrowrightBlueGray90001 =
      'assets/images/img_arrowright_blue_gray_900_01.svg';

  static String imgArrowright = 'assets/images/img_arrowright.svg';

  static String imgProfileimglarge25 =
      'assets/images/img_profileimglarge_25.png';

  static String imgProfileimglarge50x505 =
      'assets/images/img_profileimglarge_50x50_5.png';

  static String imgGrammaphone1 = 'assets/images/img_grammaphone1.png';

  static String imgEllipse1524x24 = 'assets/images/img_ellipse15_24x24.png';

  static String imgPic3 = 'assets/images/img_pic_3.png';

  static String imgProfileimglarge19 =
      'assets/images/img_profileimglarge_19.png';

  static String imgPlantrecognition879x428 =
      'assets/images/img_plantrecognition_879x428.png';

  static String imgProfileimglarge50x509 =
      'assets/images/img_profileimglarge_50x50_9.png';

  static String imgEdit24x24 = 'assets/images/img_edit_24x24.svg';

  static String imgGroup97071 = 'assets/images/img_group97071.png';

  static String imgEye16x16 = 'assets/images/img_eye_16x16.svg';

  static String imgGroup9796 = 'assets/images/img_group9796.svg';

  static String imgEllipse11118x118 = 'assets/images/img_ellipse11_118x118.png';

  static String imgEllipse316x16 = 'assets/images/img_ellipse3_16x16.png';

  static String imgAntiquetransparent =
      'assets/images/img_antiquetransparent.png';

  static String imgPic4 = 'assets/images/img_pic_4.png';

  static String imgEllipse416x16 = 'assets/images/img_ellipse4_16x16.png';

  static String imgUnsplashenrurz62wui66x663 =
      'assets/images/img_unsplashenrurz62wui_66x66_3.png';

  static String imgRectangle132182x824 =
      'assets/images/img_rectangle1321_82x82_4.png';

  static String img5162 = 'assets/images/img_5162.png';

  static String imgRectangle22130x130 =
      'assets/images/img_rectangle22_130x130.png';

  static String imgCut16x16 = 'assets/images/img_cut_16x16.svg';

  static String imgRectangle1314150x1904 =
      'assets/images/img_rectangle1314_150x190_4.png';

  static String imgEllipse360x602 = 'assets/images/img_ellipse3_60x60_2.png';

  static String imgUser24x24 = 'assets/images/img_user_24x24.svg';

  static String imgImage123 = 'assets/images/img_image123.png';

  static String imgArrowrightBlueGray6001 =
      'assets/images/img_arrowright_blue_gray_600_1.svg';

  static String imgLivestreaming = 'assets/images/img_livestreaming.png';

  static String imgRectangle460246x396 =
      'assets/images/img_rectangle460_246x396.png';

  static String imgRectangle132993x932 =
      'assets/images/img_rectangle1329_93x93_2.png';

  static String imgUser = 'assets/images/img_user.svg';

  static String imgCutWhiteA700 = 'assets/images/img_cut_white_a700.svg';

  static String imgUnsplashenrurz62wui86x864 =
      'assets/images/img_unsplashenrurz62wui_86x86_4.png';

  static String imgStar26 = 'assets/images/img_star2_6.svg';

  static String imgRectangle1314150x1901 =
      'assets/images/img_rectangle1314_150x190_1.png';

  static String imgPic1 = 'assets/images/img_pic_1.png';

  static String imgImage82 = 'assets/images/img_image82.png';

  static String imgGlobe = 'assets/images/img_globe.svg';

  static String imgRectangle11130x130 =
      'assets/images/img_rectangle11_130x130.png';

  static String imgProfileimglarge20 =
      'assets/images/img_profileimglarge_20.png';

  static String imgRectangle12130x130 =
      'assets/images/img_rectangle12_130x130.png';

  static String imgCalendarBlueGray400 =
      'assets/images/img_calendar_blue_gray_400.svg';

  static String imgVector61 = 'assets/images/img_vector61.svg';

  static String imgPlaysolidWhiteA700 =
      'assets/images/img_playsolid_white_a700.svg';

  static String imgUser16x16 = 'assets/images/img_user_16x16.svg';

  static String imgDashboard = 'assets/images/img_dashboard.svg';

  static String imgProfileimglarge35 =
      'assets/images/img_profileimglarge_35.png';

  static String imgProfileimglarge30 =
      'assets/images/img_profileimglarge_30.png';

  static String imgProfileimglarge56x562 =
      'assets/images/img_profileimglarge_56x56_2.png';

  static String imgRectangle1312160x1602 =
      'assets/images/img_rectangle1312_160x160_2.png';

  static String imgRectangle1312174x1742 =
      'assets/images/img_rectangle1312_174x174_2.png';

  static String imgThumbsup = 'assets/images/img_thumbsup.svg';

  static String imgAirplaneBlack900 =
      'assets/images/img_airplane_black_900.svg';

  static String imgRectangle20130x130 =
      'assets/images/img_rectangle20_130x130.png';

  static String imgPlaysolidBlueA7001 =
      'assets/images/img_playsolid_blue_a700_1.svg';

  static String imgForward = 'assets/images/img_forward.svg';

  static String imgEllipse7118x118 = 'assets/images/img_ellipse7_118x118.png';

  static String imgGroup10210 = 'assets/images/img_group10210.svg';

  static String imgCloseWhiteA700 = 'assets/images/img_close_white_a700.svg';

  static String imgRectangle1312160x1603 =
      'assets/images/img_rectangle1312_160x160_3.png';

  static String imgLayoutube = 'assets/images/img_layoutube.svg';

  static String imgStar17 = 'assets/images/img_star17.svg';

  static String imgUnsplashfr2iwkpsiy50x503 =
      'assets/images/img_unsplashfr2iwkpsiy_50x50_3.png';

  static String imgRectangle1319463x214 =
      'assets/images/img_rectangle1319_463x214.png';

  static String imgShare24x24 = 'assets/images/img_share_24x24.svg';

  static String imgRectangle18130x1302 =
      'assets/images/img_rectangle18_130x130_2.png';

  static String imgEnvelopealtoutline =
      'assets/images/img_envelopealtoutline.svg';

  static String imgRectangle1316416x214 =
      'assets/images/img_rectangle1316_416x214.png';

  static String imgSend = 'assets/images/img_send.svg';

  static String imgProfileimglarge27 =
      'assets/images/img_profileimglarge_27.png';

  static String imgEllipse6118x118 = 'assets/images/img_ellipse6_118x118.png';

  static String imgRectangle1330 = 'assets/images/img_rectangle1330.png';

  static String imgSearchBlueGray900 =
      'assets/images/img_search_blue_gray_900.svg';

  static String imgUnsplashfr2iwkpsiy50x504 =
      'assets/images/img_unsplashfr2iwkpsiy_50x50_4.png';

  static String img009fire2DeepOrange400 =
      'assets/images/img_009fire2_deep_orange_400.svg';

  static String imgImage122 = 'assets/images/img_image122.png';

  static String imgProfileimglarge50x508 =
      'assets/images/img_profileimglarge_50x50_8.png';

  static String imgRectangle1330250x3961 =
      'assets/images/img_rectangle1330_250x396_1.png';

  static String imgHeartoutline = 'assets/images/img_heartoutline.svg';

  static String imgProfileimglarge56x565 =
      'assets/images/img_profileimglarge_56x56_5.png';

  static String imgUiiconmoonlight = 'assets/images/img_uiiconmoonlight.svg';

  static String imgSettings = 'assets/images/img_settings.svg';

  static String imgChartsmicroBlue501 =
      'assets/images/img_chartsmicro_blue_50_1.svg';

  static String imgArrowdownBlueGray200 =
      'assets/images/img_arrowdown_blue_gray_200.svg';

  static String imgProfileimglarge37 =
      'assets/images/img_profileimglarge_37.png';

  static String imgGroup9778 = 'assets/images/img_group9778.png';

  static String imgRectangle132893x931 =
      'assets/images/img_rectangle1328_93x93_1.png';

  static String imgProfileimglarge50x5010 =
      'assets/images/img_profileimglarge_50x50_10.png';

  static String imgProfileimglarge50x5013 =
      'assets/images/img_profileimglarge_50x50_13.png';

  static String imgUnsplashenrurz62wui86x865 =
      'assets/images/img_unsplashenrurz62wui_86x86_5.png';

  static String imgPngfind55x501 = 'assets/images/img_pngfind_55x50_1.png';

  static String imgProfileimglarge56x563 =
      'assets/images/img_profileimglarge_56x56_3.png';

  static String imgCornerBlueA700 = 'assets/images/img_corner_blue_a700.svg';

  static String imgProfileimglarge50x501 =
      'assets/images/img_profileimglarge_50x50_1.png';

  static String imgEllipse750x501 = 'assets/images/img_ellipse7_50x50_1.png';

  static String imgEllipse5150x150 = 'assets/images/img_ellipse5_150x150.png';

  static String imgProfileimglarge38 =
      'assets/images/img_profileimglarge_38.png';

  static String imgRectangle14130x130 =
      'assets/images/img_rectangle14_130x130.png';

  static String imgProfileimglarge56x561 =
      'assets/images/img_profileimglarge_56x56_1.png';

  static String imgClose32x48 = 'assets/images/img_close_32x48.svg';

  static String imgArrowdownBlueGray600 =
      'assets/images/img_arrowdown_blue_gray_600.svg';

  static String imgRectangle17130x130 =
      'assets/images/img_rectangle17_130x130.png';

  static String imgCrop = 'assets/images/img_crop.svg';

  static String imgUnsplashenrurz62wui4 =
      'assets/images/img_unsplashenrurz62wui_4.png';

  static String imgProfileimglarge33 =
      'assets/images/img_profileimglarge_33.png';

  static String imgCheckmarkGreen600 =
      'assets/images/img_checkmark_green_600.svg';

  static String imgEllipse150x502 = 'assets/images/img_ellipse1_50x50_2.png';

  static String imgProfileimglarge29 =
      'assets/images/img_profileimglarge_29.png';

  static String imgArrowrightBlueGray600 =
      'assets/images/img_arrowright_blue_gray_600.svg';

  static String imgEllipse280x80 = 'assets/images/img_ellipse2_80x80.png';

  static String imgUnsplashenrurz62wui66x662 =
      'assets/images/img_unsplashenrurz62wui_66x66_2.png';

  static String imgRectangle16130x1301 =
      'assets/images/img_rectangle16_130x130_1.png';

  static String imgSearchBlueGray200 =
      'assets/images/img_search_blue_gray_200.svg';

  static String imgCalendar24x24 = 'assets/images/img_calendar_24x24.svg';

  static String imgProfileimglarge56x56 =
      'assets/images/img_profileimglarge_56x56.png';

  static String imgSignal = 'assets/images/img_signal.svg';

  static String imgRectangle19130x130 =
      'assets/images/img_rectangle19_130x130.png';

  static String imgRectangle15130x1302 =
      'assets/images/img_rectangle15_130x130_2.png';

  static String imgVolume20x20 = 'assets/images/img_volume_20x20.svg';

  static String imgPlus10x10 = 'assets/images/img_plus_10x10.svg';

  static String imgRectangle20130x1302 =
      'assets/images/img_rectangle20_130x130_2.png';

  static String imgGroup9757 = 'assets/images/img_group9757.svg';

  static String imgLivefeedcapture = 'assets/images/img_livefeedcapture.png';

  static String imgRectangle132182x822 =
      'assets/images/img_rectangle1321_82x82_2.png';

  static String imgProfileimglarge24 =
      'assets/images/img_profileimglarge_24.png';

  static String imgLinkedin11 = 'assets/images/img_linkedin_1_1.svg';

  static String imgUpload = 'assets/images/img_upload.svg';

  static String imgOverflowmenu24x24 =
      'assets/images/img_overflowmenu_24x24.svg';

  static String imgRectangle3 = 'assets/images/img_rectangle3.png';

  static String imgImage83 = 'assets/images/img_image83.png';

  static String imgEllipse1224x242 = 'assets/images/img_ellipse12_24x24_2.png';

  static String imgEllipse8118x118 = 'assets/images/img_ellipse8_118x118.png';

  static String imgConsoleplaystation52 =
      'assets/images/img_consoleplaystation52.png';

  static String imgGroup3615 = 'assets/images/img_group3615.svg';

  static String imgRectangle132793x932 =
      'assets/images/img_rectangle1327_93x93_2.png';

  static String imgLock24x24 = 'assets/images/img_lock_24x24.svg';

  static String imgRectangle458530x396 =
      'assets/images/img_rectangle458_530x396.png';

  static String imgEllipse750x504 = 'assets/images/img_ellipse7_50x50_4.png';

  static String imgRectangle12130x1301 =
      'assets/images/img_rectangle12_130x130_1.png';

  static String imgRectangle10130x1302 =
      'assets/images/img_rectangle10_130x130_2.png';

  static String imgEllipse9118x118 = 'assets/images/img_ellipse9_118x118.png';

  static String imgEllipse5418x18 = 'assets/images/img_ellipse54_18x18.png';

  static String imgProfileimglarge50x5011 =
      'assets/images/img_profileimglarge_50x50_11.png';

  static String imgImage126 = 'assets/images/img_image126.png';

  static String imgNotification = 'assets/images/img_notification.svg';

  static String imgImage80154x1904 = 'assets/images/img_image80_154x190_4.png';

  static String imgRectangle1314190x3961 =
      'assets/images/img_rectangle1314_190x396_1.png';

  static String imgRectangle11130x1303 =
      'assets/images/img_rectangle11_130x130_3.png';

  static String imgEllipse350x502 = 'assets/images/img_ellipse3_50x50_2.png';

  static String imgImage15 = 'assets/images/img_image15.png';

  static String imgRectangle12130x1303 =
      'assets/images/img_rectangle12_130x130_3.png';

  static String imgMobile = 'assets/images/img_mobile.svg';

  static String imgGroup9786 = 'assets/images/img_group9786.png';

  static String imgProfileimglarge16 =
      'assets/images/img_profileimglarge_16.png';

  static String imgPlus2 = 'assets/images/img_plus_2.svg';

  static String imgUser1 = 'assets/images/img_user_1.svg';

  static String imgArrowdownBlueGray400 =
      'assets/images/img_arrowdown_blue_gray_400.svg';

  static String imgArrowdownWhiteA700 =
      'assets/images/img_arrowdown_white_a700.svg';

  static String imgEllipse28 = 'assets/images/img_ellipse28.png';

  static String imgGroup185 = 'assets/images/img_group185.svg';

  static String imgCheck1 = 'assets/images/img_check1.png';

  static String imgCall16x16 = 'assets/images/img_call_16x16.svg';

  static String imgUnsplashfr2iwkpsiy50x501 =
      'assets/images/img_unsplashfr2iwkpsiy_50x50_1.png';

  static String imgStar171 = 'assets/images/img_star17_1.svg';

  static String imgFloatingicon = 'assets/images/img_floatingicon.svg';

  static String imgProfileimglarge23 =
      'assets/images/img_profileimglarge_23.png';

  static String imgRectangle13130x130 =
      'assets/images/img_rectangle13_130x130.png';

  static String imgVideocamera16x16 = 'assets/images/img_videocamera_16x16.svg';

  static String imgPlus = 'assets/images/img_plus.svg';

  static String imgPolygon1 = 'assets/images/img_polygon1.svg';

  static String imgFrame34688 = 'assets/images/img_frame34688.svg';

  static String imgThumbsup24x24 = 'assets/images/img_thumbsup_24x24.svg';

  static String imgAlarm = 'assets/images/img_alarm.svg';

  static String imgVolume = 'assets/images/img_volume.svg';

  static String imgSearch = 'assets/images/img_search.svg';

  static String imgEllipse350x501 = 'assets/images/img_ellipse3_50x50_1.png';

  static String imgMail = 'assets/images/img_mail.svg';

  static String imgRectangle10130x1301 =
      'assets/images/img_rectangle10_130x130_1.png';

  static String imgProfileimglarge10 =
      'assets/images/img_profileimglarge_10.png';

  static String imgProfileimglarge100x100 =
      'assets/images/img_profileimglarge_100x100.png';

  static String imgMailBlueGray400 = 'assets/images/img_mail_blue_gray_400.svg';

  static String imgRectangle19130x1302 =
      'assets/images/img_rectangle19_130x130_2.png';

  static String imgProfileimglarge32 =
      'assets/images/img_profileimglarge_32.png';

  static String imgProfileimglarge50x5012 =
      'assets/images/img_profileimglarge_50x50_12.png';

  static String imgVideocamera24x24 = 'assets/images/img_videocamera_24x24.svg';

  static String imgRectangle11130x1301 =
      'assets/images/img_rectangle11_130x130_1.png';

  static String imgAlarm26x24 = 'assets/images/img_alarm_26x24.svg';

  static String imgEllipse150x503 = 'assets/images/img_ellipse1_50x50_3.png';

  static String imgBluetoothbsolid = 'assets/images/img_bluetoothbsolid.svg';

  static String imgRectangle132993x933 =
      'assets/images/img_rectangle1329_93x93_3.png';

  static String imgFile = 'assets/images/img_file.svg';

  static String imgMenu24x24 = 'assets/images/img_menu_24x24.svg';

  static String imgProfileimglarge34 =
      'assets/images/img_profileimglarge_34.png';

  static String imgRectangle13130x1301 =
      'assets/images/img_rectangle13_130x130_1.png';

  static String imgRectangle1303 = 'assets/images/img_rectangle1303.png';

  static String imgTwitter24x24 = 'assets/images/img_twitter_24x24.svg';

  static String imgImage128 = 'assets/images/img_image128.png';

  static String imgFile26x26 = 'assets/images/img_file_26x26.svg';

  static String imgPlusWhiteA700 = 'assets/images/img_plus_white_a700.svg';

  static String imgEdit = 'assets/images/img_edit.svg';

  static String imgRectangle16130x130 =
      'assets/images/img_rectangle16_130x130.png';

  static String imgStar22 = 'assets/images/img_star2_2.svg';

  static String imgRectangle10130x1303 =
      'assets/images/img_rectangle10_130x130_3.png';

  static String imgImage81154x1901 = 'assets/images/img_image81_154x190_1.png';

  static String imgCut = 'assets/images/img_cut.svg';

  static String imgProfileimglarge17 =
      'assets/images/img_profileimglarge_17.png';

  static String imgWhatsapp = 'assets/images/img_whatsapp.svg';

  static String imgProfileimglarge50x502 =
      'assets/images/img_profileimglarge_50x50_2.png';

  static String imgCall = 'assets/images/img_call.svg';

  static String imgCalendarBlueA700 =
      'assets/images/img_calendar_blue_a700.svg';

  static String imgLocation = 'assets/images/img_location.svg';

  static String imgRectangle1305 = 'assets/images/img_rectangle1305.png';

  static String imgEllipse750x502 = 'assets/images/img_ellipse7_50x50_2.png';

  static String imgChartsmicro = 'assets/images/img_chartsmicro.svg';

  static String imgCalendar16x16 = 'assets/images/img_calendar_16x16.svg';

  static String imgInstagramoutline = 'assets/images/img_instagramoutline.svg';

  static String imgYh = 'assets/images/img_yh.svg';

  static String imgEllipse750x507 = 'assets/images/img_ellipse7_50x50_7.png';

  static String imgRectangle24130x130 =
      'assets/images/img_rectangle24_130x130.png';

  static String imgCamera = 'assets/images/img_camera.svg';

  static String imgCiairplay = 'assets/images/img_ciairplay.svg';

  static String imgGroup10451 = 'assets/images/img_group10451.svg';

  static String imgGroup10440 = 'assets/images/img_group10440.svg';

  static String imgProfileimglarge21 =
      'assets/images/img_profileimglarge_21.png';

  static String imgVectorBlueA700 = 'assets/images/img_vector_blue_a700.svg';

  static String img009fire2 = 'assets/images/img_009fire2.svg';

  static String imgEllipse6624x24 = 'assets/images/img_ellipse66_24x24.png';

  static String imgUnsplashenrurz62wui86x862 =
      'assets/images/img_unsplashenrurz62wui_86x86_2.png';

  static String imgFavorite = 'assets/images/img_favorite.svg';

  static String imgStar = 'assets/images/img_star.svg';

  static String imgCheckmark = 'assets/images/img_checkmark.svg';

  static String imgTwitter = 'assets/images/img_twitter.svg';

  static String imgProfileimglarge18 =
      'assets/images/img_profileimglarge_18.png';

  static String imgGroup9755 = 'assets/images/img_group9755.svg';

  static String imgNotification1 = 'assets/images/img_notification_1.svg';

  static String imgProfileimglarge24x24 =
      'assets/images/img_profileimglarge_24x24.png';

  static String imgGoogle = 'assets/images/img_google.svg';

  static String imgFrame81 = 'assets/images/img_frame81.png';

  static String imgCheckmark24x24 = 'assets/images/img_checkmark_24x24.svg';

  static String imgIcloud1 = 'assets/images/img_icloud1.svg';

  static String imgRectangle11130x1302 =
      'assets/images/img_rectangle11_130x130_2.png';

  static String imgRectangle18130x1301 =
      'assets/images/img_rectangle18_130x130_1.png';

  static String img009fire2DeepOrange40014x14 =
      'assets/images/img_009fire2_deep_orange_400_14x14.svg';

  static String imgTicket = 'assets/images/img_ticket.svg';

  static String imgImage80154x1901 = 'assets/images/img_image80_154x190_1.png';

  static String imgStar11 = 'assets/images/img_star1_1.svg';

  static String imgGroup3616 = 'assets/images/img_group3616.svg';

  static String imgMaskgroup159x393 = 'assets/images/img_maskgroup_159x393.png';

  static String imgCall24x24 = 'assets/images/img_call_24x24.svg';

  static String imgEllipse750x508 = 'assets/images/img_ellipse7_50x50_8.png';

  static String imgPic = 'assets/images/img_pic.png';

  static String imgRectangle21130x130 =
      'assets/images/img_rectangle21_130x130.png';

  static String imgRectangle1325 = 'assets/images/img_rectangle1325.png';

  static String imgSend48x48 = 'assets/images/img_send_48x48.svg';

  static String imgEllipse1324x252 = 'assets/images/img_ellipse13_24x25_2.png';

  static String imgVideocamera1 = 'assets/images/img_videocamera_1.svg';

  static String imgOverflowmenu1 = 'assets/images/img_overflowmenu_1.svg';

  static String imgRectangle132693x932 =
      'assets/images/img_rectangle1326_93x93_2.png';

  static String imgFlagargentina11 = 'assets/images/img_flagargentina1_1.png';

  static String imgEye = 'assets/images/img_eye.svg';

  static String imgRectangle132182x823 =
      'assets/images/img_rectangle1321_82x82_3.png';

  static String imgArrowupBlueGray600 =
      'assets/images/img_arrowup_blue_gray_600.svg';

  static String imgNotification24x24 =
      'assets/images/img_notification_24x24.svg';

  static String imgPic2 = 'assets/images/img_pic_2.png';

  static String imgStar24 = 'assets/images/img_star2_4.svg';

  static String imgGroup10281 = 'assets/images/img_group10281.svg';

  static String imgQuestion = 'assets/images/img_question.svg';

  static String imgMicrophone20x20 = 'assets/images/img_microphone_20x20.svg';

  static String imgGroup2696 = 'assets/images/img_group2696.svg';

  static String imgEllipse1324x251 = 'assets/images/img_ellipse13_24x25_1.png';

  static String imgRectangle19130x1301 =
      'assets/images/img_rectangle19_130x130_1.png';

  static String imgProfileimglarge22 =
      'assets/images/img_profileimglarge_22.png';

  static String imgUnsplashenrurz62wui66x661 =
      'assets/images/img_unsplashenrurz62wui_66x66_1.png';

  static String imgCheckmark56x56 = 'assets/images/img_checkmark_56x56.svg';

  static String imgCall64x64 = 'assets/images/img_call_64x64.svg';

  static String imgRectangle1330162x1721 =
      'assets/images/img_rectangle1330_162x172_1.png';

  static String imgEye24x24 = 'assets/images/img_eye_24x24.svg';

  static String imgImage = 'assets/images/img_image.png';

  static String imgFlagargentina132x48 =
      'assets/images/img_flagargentina1_32x48.png';

  static String imgVolumeBlueGray300 =
      'assets/images/img_volume_blue_gray_300.svg';

  static String imgCalendar = 'assets/images/img_calendar.svg';

  static String imgRectangle12130x1302 =
      'assets/images/img_rectangle12_130x130_2.png';

  static String imgRectangle1314150x1902 =
      'assets/images/img_rectangle1314_150x190_2.png';

  static String imgComputer = 'assets/images/img_computer.svg';

  static String imgEllipse2718x18 = 'assets/images/img_ellipse27_18x18.png';

  static String imgSearchBlueA100 = 'assets/images/img_search_blue_a100.svg';

  static String imgRectangle10130x130 =
      'assets/images/img_rectangle10_130x130.png';

  static String imgProfileimglarge48x48 =
      'assets/images/img_profileimglarge_48x48.png';

  static String imgFrame9929 = 'assets/images/img_frame9929.svg';

  static String imgMusicBlueGray400 =
      'assets/images/img_music_blue_gray_400.svg';

  static String imgEllipse750x505 = 'assets/images/img_ellipse7_50x50_5.png';

  static String imgTelegramalt1 = 'assets/images/img_telegramalt1.svg';

  static String imgImage81 = 'assets/images/img_image81.png';

  static String imgImage124 = 'assets/images/img_image124.png';

  static String imgProfileimglarge32x32 =
      'assets/images/img_profileimglarge_32x32.png';

  static String imgStar23 = 'assets/images/img_star2_3.svg';

  static String imgCheckbox = 'assets/images/img_checkbox.svg';

  static String imgMinimize = 'assets/images/img_minimize.svg';

  static String imgHome = 'assets/images/img_home.svg';

  static String imgMicrophone = 'assets/images/img_microphone.svg';

  static String imgArrowleftBlueGray90001 =
      'assets/images/img_arrowleft_blue_gray_900_01.svg';

  static String imgArrowupGreen600 = 'assets/images/img_arrowup_green_600.svg';

  static String imgStar1713x13 = 'assets/images/img_star17_13x13.svg';

  static String imgRectangle132893x933 =
      'assets/images/img_rectangle1328_93x93_3.png';

  static String imgMoonoutline = 'assets/images/img_moonoutline.svg';

  static String imgUnsplashenrurz62wui50x50 =
      'assets/images/img_unsplashenrurz62wui_50x50.png';

  static String imgEllipse360x601 = 'assets/images/img_ellipse3_60x60_1.png';

  static String imgStar116x16 = 'assets/images/img_star1_16x16.svg';

  static String imgArrowleftWhiteA700 =
      'assets/images/img_arrowleft_white_a700.svg';

  static String imgImage107 = 'assets/images/img_image107.png';

  static String imgUnsplashenrurz62wui86x86 =
      'assets/images/img_unsplashenrurz62wui_86x86.png';

  static String imgArrowgrowthoutlineRed700 =
      'assets/images/img_arrowgrowthoutline_red_700.svg';

  static String imgQuestion24x24 = 'assets/images/img_question_24x24.svg';

  static String imgImage127 = 'assets/images/img_image127.png';

  static String imgStar1 = 'assets/images/img_star1.svg';

  static String imgEllipse180x80 = 'assets/images/img_ellipse1_80x80.png';

  static String imgProfileimglarge72x721 =
      'assets/images/img_profileimglarge_72x72_1.png';

  static String imgFrame9866 = 'assets/images/img_frame9866.svg';

  static String imgImage4 = 'assets/images/img_image4.png';

  static String imgEllipse380x80 = 'assets/images/img_ellipse3_80x80.png';

  static String imgProfileimglarge50x507 =
      'assets/images/img_profileimglarge_50x50_7.png';

  static String imgOverflowmenu16x16 =
      'assets/images/img_overflowmenu_16x16.svg';

  static String imgBrightness = 'assets/images/img_brightness.svg';

  static String imgRectangle15130x1301 =
      'assets/images/img_rectangle15_130x130_1.png';

  static String imgClock24x24 = 'assets/images/img_clock_24x24.svg';

  static String imgOverflowmenu = 'assets/images/img_overflowmenu.svg';

  static String imageNotFound = 'assets/images/image_not_found.png';
}
