import 'package:flutter/material.dart';
import 'package:app1/core/app_export.dart';

class AppStyle {
  static TextStyle txtGilroyMedium12BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyBold36 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      36,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyRegular14Black900 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold16WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyBold16Red700 = TextStyle(
    color: ColorConstant.red700,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyBold32 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      32,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyRegular12Green600 = TextStyle(
    color: ColorConstant.green600,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold36BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      36,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtRobotoBlack1418 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      14.18,
    ),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w900,
  );

  static TextStyle txtGilroyMedium16BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold10BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      10,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular14Bluegray300 = TextStyle(
    color: ColorConstant.blueGray300,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold28Green600 = TextStyle(
    color: ColorConstant.green600,
    fontSize: getFontSize(
      28,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold14OrangeA700 = TextStyle(
    color: ColorConstant.orangeA700,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular12Bluegray90002 = TextStyle(
    color: ColorConstant.blueGray90002,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyBold18Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroySemiBold14BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtPoppinsRegular14WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyMedium14Bluegray80001 = TextStyle(
    color: ColorConstant.blueGray80001,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyBold28 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      28,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroySemiBold18Green600 = TextStyle(
    color: ColorConstant.green600,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium12Bluegray600 = TextStyle(
    color: ColorConstant.blueGray600,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyRegular16Indigo900 = TextStyle(
    color: ColorConstant.indigo900,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyMedium32 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      32,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyBold24 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      24,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtChivoRegular12 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Chivo',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyBold20 = TextStyle(
    color: ColorConstant.orangeA700,
    fontSize: getFontSize(
      20,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyMedium10Bluegray80001 = TextStyle(
    color: ColorConstant.blueGray80001,
    fontSize: getFontSize(
      10,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium16Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtPoppinsRegular14 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold14Bluegray300 = TextStyle(
    color: ColorConstant.blueGray300,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular16Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyBold8 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      8,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyMedium12Gray900a5 = TextStyle(
    color: ColorConstant.gray900A5,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold16Black900 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold18BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular12Gray900a5 = TextStyle(
    color: ColorConstant.gray900A5,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyRegular12Bluegray600 = TextStyle(
    color: ColorConstant.blueGray600,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold18Indigo900 = TextStyle(
    color: ColorConstant.indigo900,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium16Red700 = TextStyle(
    color: ColorConstant.red700,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium16Black900 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold24OrangeA700 = TextStyle(
    color: ColorConstant.orangeA700,
    fontSize: getFontSize(
      24,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold20BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      20,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium12Gray90006 = TextStyle(
    color: ColorConstant.gray90006,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyRegular14WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold16Bluegray400 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyBold18BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroySemiBold14Green600 = TextStyle(
    color: ColorConstant.green600,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtPoppinsRegular12 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyMedium14Bluegray40001 = TextStyle(
    color: ColorConstant.blueGray40001,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold24BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      24,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold16Bluegray70001 = TextStyle(
    color: ColorConstant.blueGray70001,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold24WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      24,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular14Bluegray400 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyRegular12Gray50001 = TextStyle(
    color: ColorConstant.gray50001,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyBold14Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyMedium12Bluegray300 = TextStyle(
    color: ColorConstant.blueGray300,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold14Bluegray70001 = TextStyle(
    color: ColorConstant.blueGray70001,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold18WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium16Green600 = TextStyle(
    color: ColorConstant.green600,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium14Gray400 = TextStyle(
    color: ColorConstant.gray400,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyRegular14Gray900 = TextStyle(
    color: ColorConstant.gray900,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold12 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold10 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      10,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtChivoRegular1218 = TextStyle(
    color: ColorConstant.gray50002,
    fontSize: getFontSize(
      12.18,
    ),
    fontFamily: 'Chivo',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtPoppinsRegular14Bluegray400 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyRegular12 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold18Bluegray90001 = TextStyle(
    color: ColorConstant.blueGray90001,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold18 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular10 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      10,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtJosefinSansRegular20 = TextStyle(
    color: ColorConstant.orangeA700,
    fontSize: getFontSize(
      20,
    ),
    fontFamily: 'Josefin Sans',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyRegular14Gray500 = TextStyle(
    color: ColorConstant.gray500,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold16 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular16 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyBold16Indigo900 = TextStyle(
    color: ColorConstant.indigo900,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroySemiBold14Bluegray400 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular14 = TextStyle(
    color: ColorConstant.blueGray600,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold14 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold16Blue800 = TextStyle(
    color: ColorConstant.blue800,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtRobotoRegular16 = TextStyle(
    color: ColorConstant.blueGray40002,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyMedium14Bluegray300 = TextStyle(
    color: ColorConstant.blueGray300,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium18Bluegray400 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium16WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyBold16Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroySemiBold24 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      24,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyBold36BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      36,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyRegular16Gray90008 = TextStyle(
    color: ColorConstant.gray90008,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold20 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      20,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold18Blue200 = TextStyle(
    color: ColorConstant.blue200,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular18Bluegray400 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold28 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      28,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold18Red700 = TextStyle(
    color: ColorConstant.red700,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyBold20Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      20,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtInterExtraBold16 = TextStyle(
    color: ColorConstant.gray90003,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Inter',
    fontWeight: FontWeight.w800,
  );

  static TextStyle txtGilroySemiBold28Red700 = TextStyle(
    color: ColorConstant.red700,
    fontSize: getFontSize(
      28,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular14Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyRegular18 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyMedium16Bluegray300 = TextStyle(
    color: ColorConstant.blueGray300,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium12Gray900 = TextStyle(
    color: ColorConstant.gray900,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold18Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold24Black900 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      24,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium8 = TextStyle(
    color: ColorConstant.blueGray80002,
    fontSize: getFontSize(
      8,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium16Bluegray50 = TextStyle(
    color: ColorConstant.blueGray50,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold28Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      28,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtRobotoRegular20 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      20,
    ),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyMedium14BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyRegular12Red700 = TextStyle(
    color: ColorConstant.red700,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtInterMedium18 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Inter',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold18Bluegray400 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold12Bluegray30001 = TextStyle(
    color: ColorConstant.blueGray30001,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold12BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular16Bluegray300 = TextStyle(
    color: ColorConstant.blueGray300,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyMedium14WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold12Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium10Black900 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      10,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyRegular16Bluegray700 = TextStyle(
    color: ColorConstant.blueGray700,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroySemiBold12WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold16Green600 = TextStyle(
    color: ColorConstant.green600,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold36 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      36,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium18Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium14Bluegray200 = TextStyle(
    color: ColorConstant.blueGray200,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium14Bluegray20001 = TextStyle(
    color: ColorConstant.blueGray20001,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold16Gray90005 = TextStyle(
    color: ColorConstant.gray90005,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium14Bluegray600 = TextStyle(
    color: ColorConstant.blueGray600,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold16Gray90007 = TextStyle(
    color: ColorConstant.gray90007,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold10Gray900 = TextStyle(
    color: ColorConstant.gray900,
    fontSize: getFontSize(
      10,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold16BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyBold16 = TextStyle(
    color: ColorConstant.amber500,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyBold18 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroySemiBold44 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      44,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium10Bluegray400 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      10,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyBold12 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroySemiBold24Gray90004 = TextStyle(
    color: ColorConstant.gray90004,
    fontSize: getFontSize(
      24,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium14Green600 = TextStyle(
    color: ColorConstant.green600,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyBold14 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroySemiBold12Black900 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium20 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      20,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyRegular12Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyBold10 = TextStyle(
    color: ColorConstant.gray900,
    fontSize: getFontSize(
      10,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyMedium14Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium24 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      24,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium16Bluegray600 = TextStyle(
    color: ColorConstant.blueGray600,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyBold14Black900 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyBold16BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtRubikMedium12 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Rubik',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyRegular14BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyBold18Bluegray400 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyMedium12WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyBold32WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      32,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyMedium14Black900 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyBold24Red700 = TextStyle(
    color: ColorConstant.red700,
    fontSize: getFontSize(
      24,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroyMedium12Bluegray900 = TextStyle(
    color: ColorConstant.blueGray900,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium10 = TextStyle(
    color: ColorConstant.blueGray300,
    fontSize: getFontSize(
      10,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtMontserratMedium14 = TextStyle(
    color: ColorConstant.redA200,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Montserrat',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyRegular16Bluegray200 = TextStyle(
    color: ColorConstant.blueGray200,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyMedium14 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium12 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyRegular16Bluegray600 = TextStyle(
    color: ColorConstant.blueGray600,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyMedium18 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroyMedium16 = TextStyle(
    color: ColorConstant.blueGray400,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtInterSemiBold16 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Inter',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyBold12BlueA700 = TextStyle(
    color: ColorConstant.blueA700,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroySemiBold16Indigo900 = TextStyle(
    color: ColorConstant.indigo900,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyBold18Red700 = TextStyle(
    color: ColorConstant.red700,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w700,
  );

  static TextStyle txtGilroySemiBold20Green600 = TextStyle(
    color: ColorConstant.green600,
    fontSize: getFontSize(
      20,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyMedium18Bluegray600 = TextStyle(
    color: ColorConstant.blueGray600,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold20WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      20,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroyRegular12Black90066 = TextStyle(
    color: ColorConstant.black90066,
    fontSize: getFontSize(
      12,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtGilroyMedium14Red700 = TextStyle(
    color: ColorConstant.red700,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtGilroySemiBold18Gray90003 = TextStyle(
    color: ColorConstant.gray90003,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold18Gray90002 = TextStyle(
    color: ColorConstant.gray90002,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold18Gray90001 = TextStyle(
    color: ColorConstant.gray90001,
    fontSize: getFontSize(
      18,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtGilroySemiBold14WhiteA700 = TextStyle(
    color: ColorConstant.whiteA700,
    fontSize: getFontSize(
      14,
    ),
    fontFamily: 'Gilroy',
    fontWeight: FontWeight.w600,
  );
}
