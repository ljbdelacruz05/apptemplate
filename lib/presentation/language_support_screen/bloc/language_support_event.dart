// ignore_for_file: must_be_immutable

part of 'language_support_bloc.dart';

@immutable
abstract class LanguageSupportEvent extends Equatable {}

class LanguageSupportInitialEvent extends LanguageSupportEvent {
  @override
  List<Object?> get props => [];
}

///event for change switch
class ChangeSwitchEvent extends LanguageSupportEvent {
  ChangeSwitchEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///event for change switch
class ChangeSwitch1Event extends LanguageSupportEvent {
  ChangeSwitch1Event({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}
