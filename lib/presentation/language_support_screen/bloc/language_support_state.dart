// ignore_for_file: must_be_immutable

part of 'language_support_bloc.dart';

class LanguageSupportState extends Equatable {
  LanguageSupportState({
    this.group9694Controller,
    this.isSelectedSwitch = false,
    this.isSelectedSwitch1 = false,
    this.languageSupportModelObj,
  });

  TextEditingController? group9694Controller;

  LanguageSupportModel? languageSupportModelObj;

  bool isSelectedSwitch;

  bool isSelectedSwitch1;

  @override
  List<Object?> get props => [
        group9694Controller,
        isSelectedSwitch,
        isSelectedSwitch1,
        languageSupportModelObj,
      ];
  LanguageSupportState copyWith({
    TextEditingController? group9694Controller,
    bool? isSelectedSwitch,
    bool? isSelectedSwitch1,
    LanguageSupportModel? languageSupportModelObj,
  }) {
    return LanguageSupportState(
      group9694Controller: group9694Controller ?? this.group9694Controller,
      isSelectedSwitch: isSelectedSwitch ?? this.isSelectedSwitch,
      isSelectedSwitch1: isSelectedSwitch1 ?? this.isSelectedSwitch1,
      languageSupportModelObj:
          languageSupportModelObj ?? this.languageSupportModelObj,
    );
  }
}
