import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:app1/presentation/language_support_screen/models/language_support_model.dart';part 'language_support_event.dart';part 'language_support_state.dart';class LanguageSupportBloc extends Bloc<LanguageSupportEvent, LanguageSupportState> {LanguageSupportBloc(LanguageSupportState initialState) : super(initialState) { on<LanguageSupportInitialEvent>(_onInitialize); on<ChangeSwitchEvent>(_changeSwitch); on<ChangeSwitch1Event>(_changeSwitch1); }

_changeSwitch(ChangeSwitchEvent event, Emitter<LanguageSupportState> emit, ) { emit(state.copyWith(isSelectedSwitch: event.value)); } 
_changeSwitch1(ChangeSwitch1Event event, Emitter<LanguageSupportState> emit, ) { emit(state.copyWith(isSelectedSwitch1: event.value)); } 
_onInitialize(LanguageSupportInitialEvent event, Emitter<LanguageSupportState> emit, ) async  { emit(state.copyWith(group9694Controller: TextEditingController(), isSelectedSwitch: false, isSelectedSwitch1: false)); } 
 }
