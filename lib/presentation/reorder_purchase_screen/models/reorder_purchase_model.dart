import 'package:equatable/equatable.dart';import 'listprofileimglarge5_item_model.dart';
// ignore: must_be_immutable
class ReorderPurchaseModel extends Equatable {ReorderPurchaseModel({this.listprofileimglarge5ItemList = const []});

List<Listprofileimglarge5ItemModel> listprofileimglarge5ItemList;

ReorderPurchaseModel copyWith({List<Listprofileimglarge5ItemModel>? listprofileimglarge5ItemList}) { return ReorderPurchaseModel(
listprofileimglarge5ItemList : listprofileimglarge5ItemList ?? this.listprofileimglarge5ItemList,
); } 
@override List<Object?> get props => [listprofileimglarge5ItemList];
 }
