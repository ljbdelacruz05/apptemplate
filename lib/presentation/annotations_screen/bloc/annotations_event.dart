// ignore_for_file: must_be_immutable

part of 'annotations_bloc.dart';

@immutable
abstract class AnnotationsEvent extends Equatable {}

class AnnotationsInitialEvent extends AnnotationsEvent {
  @override
  List<Object?> get props => [];
}
