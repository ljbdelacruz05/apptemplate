// ignore_for_file: must_be_immutable

part of 'annotations_bloc.dart';

class AnnotationsState extends Equatable {
  AnnotationsState({this.annotationsModelObj});

  AnnotationsModel? annotationsModelObj;

  @override
  List<Object?> get props => [
        annotationsModelObj,
      ];
  AnnotationsState copyWith({AnnotationsModel? annotationsModelObj}) {
    return AnnotationsState(
      annotationsModelObj: annotationsModelObj ?? this.annotationsModelObj,
    );
  }
}
