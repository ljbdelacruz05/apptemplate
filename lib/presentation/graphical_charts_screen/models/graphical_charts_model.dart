import 'package:equatable/equatable.dart';import 'listclose1_item_model.dart';import 'listsync_item_model.dart';
// ignore: must_be_immutable
class GraphicalChartsModel extends Equatable {GraphicalChartsModel({this.listclose1ItemList = const [], this.listsyncItemList = const []});

List<Listclose1ItemModel> listclose1ItemList;

List<ListsyncItemModel> listsyncItemList;

GraphicalChartsModel copyWith({List<Listclose1ItemModel>? listclose1ItemList, List<ListsyncItemModel>? listsyncItemList}) { return GraphicalChartsModel(
listclose1ItemList : listclose1ItemList ?? this.listclose1ItemList,
listsyncItemList : listsyncItemList ?? this.listsyncItemList,
); } 
@override List<Object?> get props => [listclose1ItemList,listsyncItemList];
 }
