import 'package:equatable/equatable.dart';import 'listpic_item_model.dart';import 'listclose_item_model.dart';
// ignore: must_be_immutable
class ElasticSearchModel extends Equatable {ElasticSearchModel({this.listpicItemList = const [], this.listcloseItemList = const []});

List<ListpicItemModel> listpicItemList;

List<ListcloseItemModel> listcloseItemList;

ElasticSearchModel copyWith({List<ListpicItemModel>? listpicItemList, List<ListcloseItemModel>? listcloseItemList}) { return ElasticSearchModel(
listpicItemList : listpicItemList ?? this.listpicItemList,
listcloseItemList : listcloseItemList ?? this.listcloseItemList,
); } 
@override List<Object?> get props => [listpicItemList,listcloseItemList];
 }
