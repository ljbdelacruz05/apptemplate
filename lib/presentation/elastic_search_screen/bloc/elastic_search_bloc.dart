import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import '../models/listpic_item_model.dart';import '../models/listclose_item_model.dart';import 'package:app1/presentation/elastic_search_screen/models/elastic_search_model.dart';part 'elastic_search_event.dart';part 'elastic_search_state.dart';class ElasticSearchBloc extends Bloc<ElasticSearchEvent, ElasticSearchState> {ElasticSearchBloc(ElasticSearchState initialState) : super(initialState) { on<ElasticSearchInitialEvent>(_onInitialize); }

List<ListpicItemModel> fillListpicItemList() { return List.generate(5, (index) => ListpicItemModel()); } 
List<ListcloseItemModel> fillListcloseItemList() { return List.generate(5, (index) => ListcloseItemModel()); } 
_onInitialize(ElasticSearchInitialEvent event, Emitter<ElasticSearchState> emit, ) async  { emit(state.copyWith(inputFieldController: TextEditingController())); emit(state.copyWith(elasticSearchModelObj: state.elasticSearchModelObj?.copyWith(listpicItemList: fillListpicItemList(), listcloseItemList: fillListcloseItemList()))); } 
 }
