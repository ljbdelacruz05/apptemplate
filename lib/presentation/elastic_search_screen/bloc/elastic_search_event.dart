// ignore_for_file: must_be_immutable

part of 'elastic_search_bloc.dart';

@immutable
abstract class ElasticSearchEvent extends Equatable {}

class ElasticSearchInitialEvent extends ElasticSearchEvent {
  @override
  List<Object?> get props => [];
}
