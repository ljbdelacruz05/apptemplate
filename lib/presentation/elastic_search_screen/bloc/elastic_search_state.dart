// ignore_for_file: must_be_immutable

part of 'elastic_search_bloc.dart';

class ElasticSearchState extends Equatable {
  ElasticSearchState({
    this.inputFieldController,
    this.elasticSearchModelObj,
  });

  TextEditingController? inputFieldController;

  ElasticSearchModel? elasticSearchModelObj;

  @override
  List<Object?> get props => [
        inputFieldController,
        elasticSearchModelObj,
      ];
  ElasticSearchState copyWith({
    TextEditingController? inputFieldController,
    ElasticSearchModel? elasticSearchModelObj,
  }) {
    return ElasticSearchState(
      inputFieldController: inputFieldController ?? this.inputFieldController,
      elasticSearchModelObj:
          elasticSearchModelObj ?? this.elasticSearchModelObj,
    );
  }
}
