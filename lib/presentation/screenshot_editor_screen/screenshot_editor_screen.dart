import 'bloc/screenshot_editor_bloc.dart';
import 'models/screenshot_editor_model.dart';
import 'package:app1/core/app_export.dart';
import 'package:app1/widgets/app_bar/appbar_image.dart';
import 'package:app1/widgets/app_bar/appbar_subtitle_2.dart';
import 'package:app1/widgets/app_bar/custom_app_bar.dart';
import 'package:flutter/material.dart';

class ScreenshotEditorScreen extends StatelessWidget {
  static Widget builder(BuildContext context) {
    return BlocProvider<ScreenshotEditorBloc>(
      create: (context) => ScreenshotEditorBloc(ScreenshotEditorState(
        screenshotEditorModelObj: ScreenshotEditorModel(),
      ))
        ..add(ScreenshotEditorInitialEvent()),
      child: ScreenshotEditorScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScreenshotEditorBloc, ScreenshotEditorState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: ColorConstant.gray50,
            appBar: CustomAppBar(
              height: getVerticalSize(
                68,
              ),
              title: AppbarSubtitle2(
                text: "lbl_done".tr,
                margin: getMargin(
                  left: 16,
                ),
              ),
              actions: [
                AppbarImage(
                  height: getVerticalSize(
                    24,
                  ),
                  width: getHorizontalSize(
                    166,
                  ),
                  svgPath: ImageConstant.imgFrame34688,
                  margin: getMargin(
                    left: 15,
                    top: 16,
                    right: 15,
                    bottom: 15,
                  ),
                ),
              ],
            ),
            body: Container(
              width: double.maxFinite,
              padding: getPadding(
                left: 16,
                top: 8,
                right: 16,
                bottom: 8,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    height: getVerticalSize(
                      726,
                    ),
                    width: getHorizontalSize(
                      396,
                    ),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        CustomImageView(
                          imagePath: ImageConstant.imgRectangle1303,
                          height: getVerticalSize(
                            726,
                          ),
                          width: getHorizontalSize(
                            396,
                          ),
                          alignment: Alignment.center,
                        ),
                        CustomImageView(
                          svgPath: ImageConstant.imgCrop,
                          height: getVerticalSize(
                            726,
                          ),
                          width: getHorizontalSize(
                            396,
                          ),
                          alignment: Alignment.center,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: getPadding(
                      left: 76,
                      top: 24,
                      bottom: 5,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: getPadding(
                            top: 5,
                          ),
                          child: Text(
                            "msg_crop_and_scale".tr,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style: AppStyle.txtGilroySemiBold18Bluegray900,
                          ),
                        ),
                        CustomImageView(
                          svgPath: ImageConstant.imgMenu24x24,
                          height: getSize(
                            24,
                          ),
                          width: getSize(
                            24,
                          ),
                          margin: getMargin(
                            left: 52,
                            bottom: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
