import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:app1/presentation/screenshot_editor_screen/models/screenshot_editor_model.dart';
part 'screenshot_editor_event.dart';
part 'screenshot_editor_state.dart';

class ScreenshotEditorBloc
    extends Bloc<ScreenshotEditorEvent, ScreenshotEditorState> {
  ScreenshotEditorBloc(ScreenshotEditorState initialState)
      : super(initialState) {
    on<ScreenshotEditorInitialEvent>(_onInitialize);
  }

  _onInitialize(
    ScreenshotEditorInitialEvent event,
    Emitter<ScreenshotEditorState> emit,
  ) async {}
}
