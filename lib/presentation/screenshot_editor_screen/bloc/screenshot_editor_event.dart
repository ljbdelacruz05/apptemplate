// ignore_for_file: must_be_immutable

part of 'screenshot_editor_bloc.dart';

@immutable
abstract class ScreenshotEditorEvent extends Equatable {}

class ScreenshotEditorInitialEvent extends ScreenshotEditorEvent {
  @override
  List<Object?> get props => [];
}
