// ignore_for_file: must_be_immutable

part of 'screenshot_editor_bloc.dart';

class ScreenshotEditorState extends Equatable {
  ScreenshotEditorState({this.screenshotEditorModelObj});

  ScreenshotEditorModel? screenshotEditorModelObj;

  @override
  List<Object?> get props => [
        screenshotEditorModelObj,
      ];
  ScreenshotEditorState copyWith(
      {ScreenshotEditorModel? screenshotEditorModelObj}) {
    return ScreenshotEditorState(
      screenshotEditorModelObj:
          screenshotEditorModelObj ?? this.screenshotEditorModelObj,
    );
  }
}
