import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import '../models/listmon_item_model.dart';import '../models/listovalcopytwo_item_model.dart';import 'package:app1/presentation/google_calender_sync_screen/models/google_calender_sync_model.dart';part 'google_calender_sync_event.dart';part 'google_calender_sync_state.dart';class GoogleCalenderSyncBloc extends Bloc<GoogleCalenderSyncEvent, GoogleCalenderSyncState> {GoogleCalenderSyncBloc(GoogleCalenderSyncState initialState) : super(initialState) { on<GoogleCalenderSyncInitialEvent>(_onInitialize); }

_onInitialize(GoogleCalenderSyncInitialEvent event, Emitter<GoogleCalenderSyncState> emit, ) async  { emit(state.copyWith(googleCalenderSyncModelObj: state.googleCalenderSyncModelObj?.copyWith(listmonItemList: fillListmonItemList(), listovalcopytwoItemList: fillListovalcopytwoItemList()))); } 
List<ListmonItemModel> fillListmonItemList() { return List.generate(2, (index) => ListmonItemModel()); } 
List<ListovalcopytwoItemModel> fillListovalcopytwoItemList() { return List.generate(3, (index) => ListovalcopytwoItemModel()); } 
 }
