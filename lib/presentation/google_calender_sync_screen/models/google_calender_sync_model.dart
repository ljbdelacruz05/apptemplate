import 'package:equatable/equatable.dart';import 'listmon_item_model.dart';import 'listovalcopytwo_item_model.dart';
// ignore: must_be_immutable
class GoogleCalenderSyncModel extends Equatable {GoogleCalenderSyncModel({this.listmonItemList = const [], this.listovalcopytwoItemList = const []});

List<ListmonItemModel> listmonItemList;

List<ListovalcopytwoItemModel> listovalcopytwoItemList;

GoogleCalenderSyncModel copyWith({List<ListmonItemModel>? listmonItemList, List<ListovalcopytwoItemModel>? listovalcopytwoItemList}) { return GoogleCalenderSyncModel(
listmonItemList : listmonItemList ?? this.listmonItemList,
listovalcopytwoItemList : listovalcopytwoItemList ?? this.listovalcopytwoItemList,
); } 
@override List<Object?> get props => [listmonItemList,listovalcopytwoItemList];
 }
